-- by Jade Lambe 
-- 06/03/15



--FUNCTION 1
--adds two numbers together and then doubles the result
add_and_double x y = (x+y)*2

--FUNCTION 2
--an infix operator called +* which does the same thing as add_and_double
(+*) :: (Num a) => a -> a -> a
(+*) x y = x `add_and_double` y
				-- Comments: Couldn't get this to work, Ronan took a look at it and told me I was missing brackets around the +*

--FUNCTION 3
--a function which takes in three arguments (a, b, and c) which are coefficients to the quadratic equation a x2 + b x + c = 0
solve_quadratic_equation :: (RealFloat a) => (a, a, a) -> (a, a)
solve_quadratic_equation (a, b, c) = (x, y) 
   where
      f = (sqrt(b^2 - (4 * a * c)))
      x = (-b + f)/2*a
      y = (-b - f)/2*a

--FUNCTION 4
--a function which takes an Int value (n) and returns a list of the first n Ints starting from 1, 
--using an infinite list and the take function.
--first_n :: (Int a) => a -> [a]
first_n :: (Num t, Enum t) => Int -> [t]
first_n n
    |n <= 0 = []
    |otherwise = take n[1..]
					--Comments: Wouldn't work with my original header, had to use :t to get header.
					
--FUNCTION 5
--Re-write first_n to a new function, first_n_integers which will take an Integer 
--argument and return a list of Integers. Do this by defining a local helper function, 
--take_integer, which takes an Integer as its first argument and a list of Integers as 
--its second argument. Use a let or a where expression to define the local helper function. 
--Note that you can use the error function to signal an error. Check that take_integer's 
--first argument is >= 0 (a pattern guard works well) and that its second argument is not an 
--empty list if its first argument is greater than 0. take_integer should be a recursive function.

take_integer :: Integer -> [Integer]
take_integer 0 = []
take_integer n = n : take_integer(n-1)

first_n_integers :: Integer -> [Integer]
first_n_integers = take_integer


--FUNCTION 6
--Define a function called double_factorial which takes an Integer n and computes 
--the product of all the factorials from 0 up to and including n. Use a let or where 
--expression to define a factorial function to be used by this function. 
--Both factorial and double_factorial will be recursive functions.
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial(n-1)

double_factorial :: (Integral a) => a -> a
double_factorial n = 2 * (n * factorial(n-1))

--FUNCTION 7
--Define an infinite list of factorials called factorials using the zipWith function.
--The first factorial should be factorial 0 i.e. 1. This can be done in one line (plus another line for the type declaration). 
--This list of factorials should have type [Integer], You can use the infinite list of factorials in its own definition. 
factorials :: [Integer]
factorials = 1 :zipWith (*) [1..] factorials

--FUNCTION 8
-- Write isPrime :: Integer -> Bool which determines whether a given integer is prime.
isPrime :: Integer -> Bool
isPrime n = checkPrime n (n-1)
    where
        checkPrime n x
            |x <= 1 = True
            |n `mod` x == 0 = False
            |otherwise = checkPrime n (x-1)
			
--FUNCTION 9
--Define primes :: [Integer] which returns the list of all primes. This is an infinite list, but because Haskell uses lazy evaluation,
--we will only generate as much of the list as we need. A call such as take 5 primes would return the first five primes. 
primes :: [Integer]
primes = filter (isPrime) [1..]
	
--FUNCTION 10
--Write a function to sum a list of integers recursively.
sum_list :: [Integer] -> Integer
sum_list [] = 0
sum_list (x:xs) = x + sum_list xs
			
--FUNCTION 11
--Write a function to sum a list of integers in terms of foldl.
sum_list_foldl :: (Num a) => [a] -> a
sum_list_foldl = foldl (+) 0
						--Comments: When looking up what folds were found that function was actually in notes already:
						--https://moodle.dkit.ie/201415/pluginfile.php/190524/mod_resource/content/1/HOF.pdf

--FUNCTION 12
--Write a function to multiply a list of integers in terms of foldr. 
mult_list_foldr :: [Integer] -> Integer
mult_list_foldr = foldr1(*)

--FUNCTION 13
--Write a function called guess takes in two arguments, a string and an int. If the string matches the string “I love functional 
--programming” and the int is less than 5 the user should be told that they have won. If the string does not match “I love functional 
--programming” but the count is less than 5 the user should be asked to guess again. If the user enters an int greater than 5 they 
--should be told that they have lost. The function should do case insensitive matching. 
guess :: String -> Int -> String
guess str x
    |str == "I love functional programming" && x < 5 = "You won"
    |str /= "I love functional programming" && x < 5 = "Guess again"
    |otherwise = "You've lost"

--FUNCTION 14
--Write a function to find the dot product of any two vectors of any length. The dot product of two vectors 
--(x1, x2, ….., xn), (y1, y2,…yn) is a scalar which is defined to be x1*y1+x2*y2+….xn*yn
findDotProduct :: (Num a) => [a] -> [a] -> a
findDotProduct xs ys = sum (zipWith (*) xs ys)
						--Comments: Found a few versions of this code on this website;
						--http://c2.com/cgi/wiki?DotProductInManyProgrammingLanguages

--FUNCTION 15
--a function such that n will be true when n is divisible by 2, and false otherwise
is_even :: (Integral n) => n -> Bool
is_even a 
    |a `mod` 2 /= 0 = False
	|otherwise = True

--FUNCTION 16
--A function unixname which removes all vowels from a string (so unixname "the house" would be "th hs").  
unixname :: String -> String
unixname [] = []
unixname xs = filter z xs 
    where
        z x = not (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u')
						--Comments: Found this one quite difficult, so Andy helped me with it
		
--FUNCTION 17
--A function intersection, which given two lists calculates the intersection (the list containing only those elements in both lists). 
intersection :: [Integer] -> [Integer] -> [Integer]
intersection xs ys = [x | x <- xs, y <- ys, x == y]
						--Comments: Found this link helpful, had to change code a little in order to return a list
						--http://stackoverflow.com/questions/8518940/haskell-number-of-matches-between-two-lists-of-ints

--FUNCTION 18
--A function censor which will replace all vowels in a string with the letter x (so censor "the house" would be thx hxxsx) 
--functionCensor :: String -> String
functionCensor :: [Char] -> [Char]
functionCensor [] = []
functionCensor (x:xs)
    |(x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u') = 'x':(functionCensor xs) 
    |otherwise = x:(functionCensor xs) 
